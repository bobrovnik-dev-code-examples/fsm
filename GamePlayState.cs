﻿namespace FSM
{
    public class GameplayState : StateBase, IGameState
    {
        public StateType StateType => StateType.Gameplay; 
        
        public GameplayState(GameStateMachine gameStateMachine) : base(gameStateMachine)
        {
        }
        
        public void Enter()
        {
        }

        public void Exit()
        {
        }
    }
}
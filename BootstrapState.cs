﻿namespace FSM
{
    public class BootstrapState : StateBase, IGameState
    {
        public StateType StateType => StateType.Bootstrap;

        public BootstrapState(GameStateMachine gameStateMachine) : base(gameStateMachine)
        {
        }
        
        public void Enter()
        {
        }

        public void Exit()
        {
        }
    }
}
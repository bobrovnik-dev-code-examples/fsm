﻿namespace FSM
{
    public abstract class StateBase
    {
        protected GameStateMachine _gameStateMachine;
        
        protected StateBase(GameStateMachine gameStateMachine)
        {
            _gameStateMachine = gameStateMachine;
        }
    }
}
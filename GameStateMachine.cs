﻿using System.Collections.Generic;

namespace FSM
{
    public class GameStateMachine
    {
        private Dictionary<StateType, IGameState> _states = new Dictionary<StateType, IGameState>();

        private IGameState _currentState;

        public GameStateMachine()
        {
            _states.Add(StateType.Bootstrap, new BootstrapState(this));
            _states.Add(StateType.Menu, new MenuState(this));
            _states.Add(StateType.Gameplay, new GameplayState(this));
        }

        public void ChangeState(IGameState newState)
        {
            _currentState?.Exit();
            _currentState = newState;
            _currentState?.Enter();
        }
    }

}
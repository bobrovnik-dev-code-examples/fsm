﻿namespace FSM
{
    public enum StateType
    {
        Bootstrap,
        Menu,   
        Gameplay
    }

    public interface IGameState
    {
        StateType StateType { get; }
        void Enter();
        void Exit();
    }
}
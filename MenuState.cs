﻿namespace FSM
{
    public class MenuState : StateBase, IGameState
    {
        public StateType StateType => StateType.Menu; 
        
        public MenuState(GameStateMachine gameStateMachine) : base(gameStateMachine)
        {
        }
        
        public void Enter()
        {
        }

        public void Exit()
        {
        }
        
    }
}